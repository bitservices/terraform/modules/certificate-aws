###############################################################################
# Optional Variables
###############################################################################

variable "route53_records_ttl" {
  type        = number
  default     = 60
  description = "The Time To Live of the Route53 records used to validate this ACM certificate."
}
variable "route53_records_force" {
  type        = bool
  default     = true
  description = "Allow creation of this record in Terraform to overwrite an existing record, if any exist."
}

###############################################################################
# Resources
###############################################################################

resource "aws_route53_record" "scope" {
  for_each = {
    for domain_validation_option in aws_acm_certificate.scope.domain_validation_options : domain_validation_option.domain_name => {
      name  = domain_validation_option.resource_record_name
      type  = domain_validation_option.resource_record_type
      value = domain_validation_option.resource_record_value
    }
  }

  ttl             = var.route53_records_ttl
  name            = each.value.name
  type            = each.value.type
  zone_id         = data.aws_route53_zone.scope.id
  records         = tolist([each.value.value])
  allow_overwrite = var.route53_records_force
}

###############################################################################
# Outputs
###############################################################################

output "route53_records_ttl" {
  value = var.route53_records_ttl
}

###############################################################################

output "route53_records_names" {
  value = values(aws_route53_record.scope)[*].name
}

output "route53_records_types" {
  value = values(aws_route53_record.scope)[*].type
}

###############################################################################
