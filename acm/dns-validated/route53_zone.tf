###############################################################################
# Optional Variables
###############################################################################

variable "route53_zone_base" {
  type        = string
  default     = "bitservices.io"
  description = "The DNS zone where Route53 records will be created to validate this ACM certificate."
}

variable "route53_zone_private" {
  type        = bool
  default     = false
  description = "Used with 'route53_zone_base' to find a private Hosted Zone. Must be 'false' for certificate validation."
}

variable "route53_zone_vpc_prefix" {
  type        = bool
  default     = false
  description = "Should 'vpc' be automatically prefixed to 'route53_zone_base'. Only works if 'vpc' is specified."
}

###############################################################################
# Locals
###############################################################################

locals {
  route53_zone_name       = format("%s%s", local.route53_zone_vpc_prefix, var.route53_zone_base)
  route53_zone_vpc_prefix = var.route53_zone_vpc_prefix && var.vpc != null ? format("%s.", var.vpc) : ""
}

###############################################################################
# Data Sources
###############################################################################

data "aws_route53_zone" "scope" {
  name         = local.route53_zone_name
  private_zone = var.route53_zone_private
}

###############################################################################
# Outputs
###############################################################################

output "route53_zone_base" {
  value = var.route53_zone_base
}

output "route53_zone_vpc_prefix" {
  value = var.route53_zone_vpc_prefix && var.vpc != null
}

###############################################################################

output "route53_zone_id" {
  value = data.aws_route53_zone.scope.id
}

output "route53_zone_name" {
  value = data.aws_route53_zone.scope.name
}

output "route53_zone_private" {
  value = data.aws_route53_zone.scope.private_zone
}

###############################################################################
