<!---------------------------------------------------------------------------->

# acm/dns-validated

#### Provides an [ACM] certificate that has been validated by using [Route53] DNS

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/certificate/aws//acm/dns-validated`**

-------------------------------------------------------------------------------

**NOTE**: If the certificate is for use with [CloudFront], this module
requires that the **aws** provider is configured for region `us-east-1`. For
details on how to call individual modules with specific providers, please
[see here](https://www.terraform.io/upgrade-guides/0-11.html#interactions-between-providers-and-modules).

-------------------------------------------------------------------------------

### Example Usage

```
module "my_dns_validated_cert" {
  source      = "gitlab.com/bitservices/certificate/aws//acm/dns-validated"
  class       = "foobar"
  owner       = "terraform@bitservices.io"
  company     = "BITServices Ltd"
  domain_name = "foobar.bitservices.io"
}
```

<!---------------------------------------------------------------------------->

[ACM]:        https://aws.amazon.com/certificate-manager/
[Route53]:    https://aws.amazon.com/route53/
[CloudFront]: https://aws.amazon.com/cloudfront/

<!---------------------------------------------------------------------------->
