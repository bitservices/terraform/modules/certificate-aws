###############################################################################
# Required Variables
###############################################################################

variable "class" {
  type        = string
  description = "The name of the asset or collection that the asset belongs."
}

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

variable "domain_name" {
  type        = string
  description = "A domain name for which the certificate should be issued."
}

###############################################################################
# Optional Variables
###############################################################################

variable "vpc"    {
  type        = string
  default     = null
  description = "The name of the VPC to which this asset belongs."
}

variable "channel"     {
  type        = string
  default     = null
  description = "The project/business unit to which this asset belongs."
}

variable "environment" {
  type        = string
  default     = null
  description = "The infrastructure environment to which this asset belongs."
}

###############################################################################

variable "alternate_domain_names" {
  type        = list(string)
  default     = []
  description = "A list of additional domain names this ACM certificate should be valid for."
}

###############################################################################

variable "dns_validation_method" {
  type        = string
  default     = "DNS"
  description = "The DNS validation method identifier. Must be 'DNS'."
}

###############################################################################
# Locals
###############################################################################

locals {
  name             = format("%s%s%s%s", var.class, var.environment != null ? "-${var.environment}" : "", var.channel != null ? "-${var.channel}" : "", var.vpc != null ? "-${var.vpc}" : "")
  all_domain_names = concat(tolist([var.domain_name]), var.alternate_domain_names)
}

###############################################################################
# Resources
###############################################################################

resource "aws_acm_certificate" "scope" {
  domain_name               = var.domain_name
  validation_method         = var.dns_validation_method
  subject_alternative_names = var.alternate_domain_names

  tags = {
    Name    = local.name
    Owner   = var.owner
    Class   = var.class
    Region  = data.aws_region.scope.name
    Company = var.company
  }
}

###############################################################################
# Outputs
###############################################################################

output "class" {
  value = var.class
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

output "domain_name" {
  value = var.domain_name
}

###############################################################################

output "vpc" {
  value = var.vpc
}

output "channel" {
  value = var.channel
}

output "environment" {
  value = var.environment
}

###############################################################################

output "alternate_domain_names" {
  value = var.alternate_domain_names
}

###############################################################################

output "dns_validation_method" {
  value = var.dns_validation_method
}

###############################################################################

output "name" {
  value = local.name
}

output "all_domain_names" {
  value = local.all_domain_names
}

###############################################################################

output "id" {
  value = aws_acm_certificate.scope.id
}

output "arn" {
  value = aws_acm_certificate.scope.arn
}

###############################################################################
