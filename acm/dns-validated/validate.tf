###############################################################################
# Resources
###############################################################################

resource "aws_acm_certificate_validation" "scope" {
  certificate_arn         = aws_acm_certificate.scope.arn
  validation_record_fqdns = values(aws_route53_record.scope)[*].fqdn
}

###############################################################################
# Outputs
###############################################################################

output "validated_certificate_arn" {
  value = aws_acm_certificate_validation.scope.certificate_arn
}

###############################################################################
