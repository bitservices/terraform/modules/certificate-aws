###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "The full name of this IAM certificate."
}

###############################################################################

variable "key" {
  type        = string
  description = "The name of the private key for this certificate."
}

variable "base" {
  type        = string
  description = "The base location of the key and certificates."
}

variable "chain" {
  type        = string
  description = "The name of the intermediate certificates for this certificate."
}

variable "certificate" {
  type        = string
  description = "The name of the certificate."
}

###############################################################################
# Optional Variables
###############################################################################

variable "key_suffix" {
  type        = string
  default     = ".key"
  description = "The file extension of the private key."
}

variable "chain_suffix" {
  type        = string
  default     = ".pem"
  description = "The file extension of the intermediates."
}

variable "certificate_suffix" {
  type        = string
  default     = ".crt"
  description = "The file extension of the certificate."
}

###############################################################################

variable "iam_path" {
  type        = string
  default     = "/"
  description = "The IAM path for the server certificate."
}

variable "cloudfront" {
  type        = bool
  default     = false
  description = "Is this certificate for use with a Cloudfront distribution."
}

###############################################################################
# Locals
###############################################################################

locals {
  iam_path         = var.cloudfront ? format("/cloudfront%s", var.iam_path) : var.iam_path
  key_path         = format("%s/%s%s", var.base, var.key, var.key_suffix)
  chain_path       = format("%s/%s%s", var.base, var.chain, var.chain_suffix)
  certificate_path = format("%s/%s%s", var.base, var.certificate, var.certificate_suffix)
}

###############################################################################
# Resources
###############################################################################

resource "aws_iam_server_certificate" "scope" {
  name              = var.name
  path              = local.iam_path
  private_key       = file(local.key_path)
  certificate_body  = file(local.certificate_path)
  certificate_chain = file(local.chain_path)
}

###############################################################################
# Outputs
###############################################################################

output "key" {
  value = var.key
}

output "base" {
  value = var.base
}

output "chain" {
  value = var.chain
}

output "certificate" {
  value = var.certificate
}

###############################################################################

output "key_suffix" {
  value = var.key_suffix
}

output "chain_suffix" {
  value = var.chain_suffix
}

output "certificate_suffix" {
  value = var.certificate_suffix
}

###############################################################################

output "key_path" {
  value = local.key_path
}

output "chain_path" {
  value = local.chain_path
}

output "certificate_path" {
  value = local.certificate_path
}

###############################################################################

output "iam_path" {
  value = local.iam_path
}

output "cloudfront" {
  value = var.cloudfront
}

###############################################################################

output "id" {
  value = aws_iam_server_certificate.scope.id
}

output "arn" {
  value = aws_iam_server_certificate.scope.arn
}

output "name" {
  value = aws_iam_server_certificate.scope.name
}

###############################################################################
