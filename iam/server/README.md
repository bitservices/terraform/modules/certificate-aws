<!---------------------------------------------------------------------------->

# iam/server

#### Manage [IAM] server certificates

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/certificate/aws//iam/server`**

-------------------------------------------------------------------------------

### Example Usage

#### Prerequisites:
All of the required components of the SSL certificate are in a folder called
**tls** in your current working directory.
```
├── tls
│   ├── test.crt
│   ├── test.key
│   └── test.pem
```

#### Usage:
```
module "my_iam_certificate" {
  source      = "gitlab.com/bitservices/certificate/aws//iam/server"
  key         = "test"
  base        = "tls"
  name        = "foo-bar"
  chain       = "test"
  certificate = "test"
}
```

<!---------------------------------------------------------------------------->

[IAM]: https://aws.amazon.com/iam

<!---------------------------------------------------------------------------->
