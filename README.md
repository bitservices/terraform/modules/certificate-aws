<!---------------------------------------------------------------------------->

# certificate (aws)

<!---------------------------------------------------------------------------->

## Description

Manages certificates on [AWS] with either [AWS] Certificate Manager ([ACM]) or
Identity and Access Management ([IAM]).

<!---------------------------------------------------------------------------->

## Modules

* [acm/dns-validated](acm/dns-validated/README.md) - Provides an [ACM] certificate that has been validated by using [Route53] DNS.
* [iam/server](iam/server/README.md) - Manage [IAM] server certificates.

<!---------------------------------------------------------------------------->

[AWS]:     https://aws.amazon.com/
[ACM]:     https://aws.amazon.com/certificate-manager/
[IAM]:     https://aws.amazon.com/iam/
[Route53]: https://aws.amazon.com/route53/

<!---------------------------------------------------------------------------->
